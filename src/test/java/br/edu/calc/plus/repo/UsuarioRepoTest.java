/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.util.Optional;
import javax.validation.ValidationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author Ronny
 */
@DataJpaTest
@ActiveProfiles("test")
public class UsuarioRepoTest {
    
    @Autowired
    private UsuarioRepo userRepo;
    
    private static Usuario usr =  new Usuario(1, "Daves Martins", "daves", "daves@daves", new BCryptPasswordEncoder().encode( "123456" ), "JF", 
                		LocalDate.now().minusYears(30));  
    
    public UsuarioRepoTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
 
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    @DisplayName("Testa a busca de usuário pelo nome")
    public void testaBuscarUsuarioCadastradoPeloNome() {
        Usuario usuarioEsperado = this.usr;
        
        Usuario usuarioObtido = userRepo.findByNome(this.usr.getNome()).get();
        
        assertEquals(usuarioEsperado, usuarioObtido);        
    }
    
    @Test
    @DisplayName("Testa a busca de usuário pelo login")
    public void testaBuscarUsuarioCadastradoPeloLogin() {
        Usuario usuarioEsperado = this.usr;
        
        Usuario usuarioObtido = userRepo.findByLogin(this.usr.getLogin()).get();
        
        assertEquals(usuarioEsperado, usuarioObtido);        
    }
    
    @Test
    @DisplayName("Testa salvar usuários diferentes com o mesmo login")
    public void testaSalvarUsuarioComOMesmoLogin() {
        
        Usuario usr1 = new Usuario(1, "ze", "ze", "ze@ze.com", "1234", "jf", LocalDate.of(2017, 2, 13));
        
        try {
            userRepo.save(usr);
        } catch (Exception e) {
        }
        
        Usuario usr2 =  new Usuario(null, "Daves Martins", "ze", "daves@daves", new BCryptPasswordEncoder().encode( "123456" ), "JF", 
                		LocalDate.now().minusYears(30));         
        try {
            userRepo.save(usr);            
        } catch (ValidationException e) {
            assertEquals("Já existe um usuário cadastrado com esse login, tente informar outro login!", e.getMessage());
        }      
                     
        Usuario usrEsperado = usr1;
        
        Usuario usuarioObtido = userRepo.findByLogin(this.usr.getLogin()).get();
        
        assertEquals(usrEsperado, usuarioObtido);        
    }
    
    
}
